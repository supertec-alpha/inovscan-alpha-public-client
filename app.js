/**
 * InovBox Updater Server - Application entry point
 *
 * InovShop/Supertec - DevForceOne Team
 *
 * Node.JS 8.x
 *
 * @author Jonathan 'Man' S. Hopper <hopper@enterthecode.io>
 *                                  <manh-tuong.nguyen@inovshop.com>
 * @version 1.0
 */
"use strict";

// Load environment configuration
require('dotenv').config();

// Includes
const Express = require('express');
const Provider = require('./modules/Provider');
const Log = require('./modules/Log');
let HTTP = require('./modules/HTTP');

// Create application instance
let app = Express();

// Configure Twig template engine
app.set('views', './application/views')
app.set('view engine', 'twig')

Log.info('Hello and welcome to Inovscan !');

// Initial fetch from provider
function init(){
    Provider.init(true)
    .then(data => {
        Log.success('Products fetched from API!');
        setInterval(() => {
            Provider.init(false)
            .then(() => Log.success('Products fetched from API!'))
            .catch( error => Log.error('Failed to retrieve products from API: ' + error));
        }, process.env.REFRESH * 1000);      
    })
    .catch(error => {
        Log.error('Failed to retrieve products from API: ' + error);
        setTimeout(() => init(), 2500);
    })
};
init();

// Initialize HTTP server
HTTP.initialize(app);

// Start listening
HTTP.listen(app);

// Handle SIGINT (Ctrl+C)
process.on('SIGINT', () => {
    process.exit();
});
