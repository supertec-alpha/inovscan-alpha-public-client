/**
 * Provider Module : Get data from O2Hub API and write it to disk
 * @navelencia 10.09.17
 */

require('dotenv').config();

const axios = require('axios');
const fs = require('fs');
const Log = require('./Log');
const path = require('path');
const download = require('download');
const del = require('del');
const isOnline = require('is-online');


// Basic GETs from API
var getProducts = function () {
    return axios.get(`${process.env.PROVIDER}/products/get`);
}
var getProduct = function (barcode) {
    return axios.get(`${process.env.PROVIDER}/products/get/${barcode}`);
}

// Save fetched data to disk
var saveDataToDisk = function (data, filename) {
    return new Promise((resolve, reject) => {
        let writePath = path.join(__dirname, '../', 'data', filename);
        fs.writeFile(writePath, data, (err) => {
            if (err)
                reject(err);
            resolve();
        });
    })
}

var init = function (downloadForced) {

    return new Promise((resolve, reject) => {
        getProducts().then(res => {
                if (downloadForced) {
                    del([path.join(__dirname, '../', 'public', 'static', 'img', 'assets', '**')]).then(paths => {});
                }        
                let products = [];
                for (let product of res.data) {
                    let pictureFilename = product.barcode + '.' + product.picture.replace(/^.+[\W]/, '');
                    let brandFilename = product.brand_logo.substring(product.brand_logo.lastIndexOf('/') + 1);

                    let formattedProduct = {
                        barcode: product.barcode,
                        name: product.name,
                        description: product.description,
                        img: 'static/img/assets/' + pictureFilename,
                        brandLogo: 'static/img/assets/' + brandFilename,
                        brand: product.brand
                    }
                    products.push(formattedProduct);

                    let pictureExists = fs.existsSync(path.join(__dirname, '../', 'public', 'static', 'img', 'assets', pictureFilename));
                    let brandExists = fs.existsSync(path.join(__dirname, '../', 'public', 'static', 'img', 'assets', brandFilename));

                    if (!pictureExists) {
                        download(product.picture, path.join(__dirname, '../', 'public', 'static', 'img', 'assets'), {
                                filename: pictureFilename
                            })
                            .then(() => {
                                Log.info(`'${pictureFilename}' was successfuly downloaded.`);
                            })
                            .catch((err) => {
                                products[products.indexOf(formattedProduct)].img = product.picture;
                                Log.error(`'${pictureFilename}' was not downloaded : ${err}. Product saved with remote image.`);
                            });
                    }

                    if (!brandExists) {
                        download(product.brand_logo, path.join(__dirname, '../', 'public', 'static', 'img', 'assets'), {
                                brandFilename
                            })
                            .then(() => {
                                Log.info(`'${brandFilename}' was successfuly downloaded.`);
                            })
                            .catch((err) => {
                                products[products.indexOf(formattedProduct)].brandLogo = product.brand_logo;
                                Log.error(`'${brandFilename}' was not downloaded : ${err}. Product's brand saved with remote image.`);
                            });
                    }
                }
                saveDataToDisk(JSON.stringify(products), 'products.json').then(() => {
                        resolve();
                    })
                    .catch(error => reject(error))
            })
            .catch(error => reject(error));
    });


}

module.exports = {
    getProducts,
    getProduct,
    init
}