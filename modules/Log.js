/**
 * InovBox Updater Server - Log module
 *
 * InovShop/Supertec - DevForceOne Team
 *
 * Node.JS 8.x
 *
 * @author Jonathan 'Man' S. Hopper <hopper@enterthecode.io>
 *                                  <manh-tuong.nguyen@inovshop.com>
 * @version 1.0
 */
"use strict";

// Includes
require('colors')
const Util = require('util')
const FSIO = require('./FSIO')

// Log channels
let channels = {
    debug: "DEBUG".grey,
    success: "SUCCESS".green,
    info: "INFO".cyan,
    warn: "WARN".yellow,
    error: "ERROR".red
}

// Log file stream
let logOut = FSIO.openLogStream('stdout.log')
let logErr = FSIO.openLogStream('stderr.log')


// Return current timestamp in local format
function getTimestamp() {
    return '[' + new Date().toLocaleString("fr-FR", {hour12: false}).grey + ']'
}

// Add entry to standard log
function log(channel, value) {
    let logEntry = getTimestamp() + '[' + channel + '] '  + ((typeof value === 'object') ? Util.inspect(value, { colors: true }) : value)

    // If console detected, print into
    if (console)
        console.log(logEntry);

    // Print to standard log file
    logOut.write(logEntry + '\n')
}

// Add entry to error log
function error(channel, value) {
    let logEntry = getTimestamp() + '[' + channel + '] '  + ((typeof value === 'object') ? Util.inspect(value, { colors: true }) : value)

    // If console detected, print into
    if (console)
        console.log(logEntry);

    // Print to standard log file
    logErr.write(logEntry + '\n')
}

module.exports = {
    debug: (value) => {
        log(channels.debug, value);
    },
    success: (value) => {
        log(channels.success, value);
    },
    info: (value) => {
        log(channels.info, value);
    },
    warn: (value) => {
        error(channels.warn, value);
    },
    error: (value) => {
        error(channels.error, value);
    }
};
