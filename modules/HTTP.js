/**
 * InovBox Updater Server - HTTP2 server module
 *
 * InovShop/Supertec - DevForceOne Team
 *
 * Node.JS 8.x
 *
 * @author Jonathan 'Man' S. Hopper <hopper@enterthecode.io>
 *                                  <manh-tuong.nguyen@inovshop.com>
 * @version 1.0
 */
"use strict";

// Includes
const Express = require('express')
const Morgan = require('morgan')
const FSIO = require('./FSIO')
const Log = require('./Log')

// Initialize HTTP server and its binding with ExpressJS
function initialize(app) {
    // Load logger
    app.use(Morgan(process.env.LOG_FORMAT, {
        stream: FSIO.openLogStream('access.log')
    }))

    // Load routes
    loadRoutes(app)
}

// Open listening socket
function listen(app) {
    app.listen(process.env.HOSTPORT, process.env.HOSTNAME, process.env.HOSTQUEUE, () => {
        Log.success('HTTP server started listening on http://' + process.env.HOSTNAME + ':' + process.env.HOSTPORT)
    })
}

// Initialise route middlewares
function loadRoutes(app) {
    // Serve static assets
    app.use('/static', Express.static(FSIO.getAppRoot() + '/public/static'))

    // Load routes from Router module
    app.use(require('./Router'))

    // Handle HTTP 404 (Not found)
    app.use((req, res) => {
        res.status(404).render('error', {
            redirect: {
                URI: '/',
                timeout: 2000
            }
        })
    })

    // Handle other errors
    app.use((err, req, res, next) => {
        res.status(err.status || 500).render('error')
    })
}

module.exports = {
    initialize,
    listen
}
