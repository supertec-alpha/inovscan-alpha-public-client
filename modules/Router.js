/**
 * InovBox Updater Server - Request router module
 *
 * InovShop/Supertec - DevForceOne Team
 *
 * Node.JS 8.x
 *
 * @author Jonathan 'Man' S. Hopper <hopper@enterthecode.io>
 *                                  <manh-tuong.nguyen@inovshop.com>
 * @version 1.0
 */
"use strict";

// Create router
let Router = require('express').Router()
let Product = require('./Product')

// Home page
Router.get('/', (req, res) => {
    res.render('index')
})

// Product page
Router.get('^/product/:product', (req, res) => {
    // Get product info from database
    let productInfo = Product.getProductInfo(req.params.product)

    // No info, 'no product' page
    if (productInfo === null) {
        res.render('noproduct', {
            redirect: {
                URI: '/',
                timeout: 5000
            }
        })
        return
    }

    res.render('product', {
        product: productInfo,
        redirect: {
            URI: '/',
            timeout: 20000
        }
    })
})

module.exports = Router
