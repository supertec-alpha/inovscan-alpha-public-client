/**
 * InovBox Updater Server - Product database module
 *
 * InovShop/Supertec - DevForceOne Team
 *
 * Node.JS 8.x
 *
 * @author Jonathan 'Man' S. Hopper <hopper@enterthecode.io>
 *                                  <manh-tuong.nguyen@inovshop.com>
 * @version 1.0
 */
"use strict";

// Includes
const FSIO = require('./FSIO')
const DB = require(FSIO.getAppRoot() + '/data/products.json')

function getProductInfo(barcode) {
    // Browse products database
    for (let i = 0; i < DB.length; ++i) {
        // Match product by barcode
        if (DB[i].barcode === barcode)
            return DB[i]
    }

    // No result
    return null
}

module.exports = {
    getProductInfo
}
