/**
 * InovBox Updater Server - FileSystem I/O module
 *
 * InovShop/Supertec - DevForceOne Team
 *
 * Node.JS 8.x
 *
 * @author Jonathan 'Man' S. Hopper <hopper@enterthecode.io>
 *                                  <manh-tuong.nguyen@inovshop.com>
 * @version 1.0
 */
"use strict";

// Includes
const FS = require('fs')
const Path = require('path')
const RFS = require('rotating-file-stream')

// Return application root directory
function getAppRoot() {
    return Path.resolve(__dirname + '/../')
}

function exists(path) {
    try {
        FS.accessSync(path, FS.constants.F_OK)
    } catch (err) {
        return false
    }

    return true
}

// Synchronously read file content (from application root)
function readSync(filePath) {
    // Test READ right
    FS.accessSync(filePath, FS.constants.R_OK)

    // Read and return file content
    return FS.readFileSync(Path.resolve(getAppRoot(), filePath))
}

// Open file stream for writing (from application root)
function openWriteStream(filePath, flags) {
    // Test if file already exists
    if (exists(filePath))
        FS.accessSync(filePath, FS.constants.W_OK)

    // Create stream for writing
    return FS.createWriteStream(Path.resolve(getAppRoot(), filePath), {
        flags: flags,
        defaultEncoding: 'utf8',
        mode: 0o640
    })
}

function openLogStream(filename) {
    // Resolve absolute file path
    let filePath = getAppRoot() + '/logs/' + filename

    // Test if file already exists
    if (exists(filePath))
        FS.accessSync(filePath, FS.constants.W_OK)

    // Create rotating file stream
    return RFS((time, index) => {
            if (!index)
                return filename

            return filename + '.' + index + '.gzip'
        },
        {
            path: getAppRoot() + '/logs',
            size: process.env.LOG_SIZE_LIMIT,
            maxSize: process.env.LOG_ARCHIVE_SIZE_LIMIT,
            mode: 0o640,
            compress: 'gzip',
            history: getAppRoot() + '/logs/' + filename + '.list'
        })
}

module.exports = {
    getAppRoot,
    exists,
    readSync,
    openWriteStream,
    openLogStream
}
